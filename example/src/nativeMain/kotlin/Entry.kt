
import gtk.*
import org.gtk.dsl.gtk.box
import org.gtk.dsl.gtk.frame
import org.gtk.gtk.common.enums.InputPurpose
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.widgets.entry.Entry
import org.gtk.gtk.widgets.windows.Window

/**
 * Test an [Entry] purpose
 */
internal fun Window.entryPurposeTest() =
	box(Orientation.VERTICAL, 10) {
		frame("Free form") {
			Entry().apply {
				inputPurpose = InputPurpose.FREE_FORM
			}.also {
				append(it)
			}
		}
		frame("Alpha") {
			Entry().apply {
				inputPurpose = InputPurpose.ALPHA
			}.also {
				append(it)
			}
		}
		frame("Digits") {
			Entry().apply {
				inputPurpose = InputPurpose.DIGITS
			}.also {
				append(it)
			}
		}
		frame("Number") {
			Entry().apply {
				inputPurpose = InputPurpose.NUMBER
			}.also {
				append(it)
			}
		}
		frame("Phone") {
			Entry().apply {
				inputPurpose = InputPurpose.PHONE
			}.also {
				append(it)
			}
		}
		frame("URL") {
			Entry().apply {
				inputPurpose = InputPurpose.URL
			}.also {
				append(it)
			}
		}
		frame("Email") {
			Entry().apply {
				inputPurpose = InputPurpose.FREE_FORM
			}.also {
				append(it)
			}
		}
		frame("Name") {
			Entry().apply {
				inputPurpose = InputPurpose.NAME
			}.also {
				append(it)
			}
		}
		frame("Password") {
			Entry().apply {
				inputPurpose = InputPurpose.PASSWORD
			}.also {
				append(it)
			}
		}
		frame("Pin") {
			Entry().apply {
				inputPurpose = InputPurpose.PIN
			}.also {
				append(it)
			}
		}
		frame("Terminal") {
			Entry().apply {
				inputPurpose = InputPurpose.TERMINAL
			}.also {
				append(it)
			}
		}
	}

/**
 * Test an [Entry]s hints
 */
internal fun Window.entryHintTest() =
	box(Orientation.VERTICAL, 10) {
		frame("Spellcheck") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_SPELLCHECK
			}.also {
				append(it)
			}
		}
		frame("No Spellcheck") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_NO_SPELLCHECK
			}.also {
				append(it)
			}
		}
		frame("Word completion") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_WORD_COMPLETION
			}.also {
				append(it)
			}
		}
		frame("Lowercase") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_LOWERCASE
			}.also {
				append(it)
			}
		}
		frame("Uppercase chars") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_UPPERCASE_CHARS
			}.also {
				append(it)
			}
		}
		frame("Uppercase words") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_UPPERCASE_WORDS
			}.also {
				append(it)
			}
		}
		frame("Uppercase sentence") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_UPPERCASE_SENTENCES
			}.also {
				append(it)
			}
		}
		frame("Inhibit OSK") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_INHIBIT_OSK
			}.also {
				append(it)
			}
		}
		frame("Vertical Writing") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_VERTICAL_WRITING
			}.also {
				append(it)
			}
		}
		frame("Emoji") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_EMOJI
			}.also {
				append(it)
			}
		}
		frame("No emoji") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_NO_EMOJI
			}.also {
				append(it)
			}
		}

		frame("Emoji & Spellcheck & Uppercase sentences") {
			Entry().apply {
				inputHints = GTK_INPUT_HINT_EMOJI + GTK_INPUT_HINT_SPELLCHECK + GTK_INPUT_HINT_UPPERCASE_SENTENCES
			}.also {
				append(it)
			}
		}

	}

/**
 * Test an [Entry]s callbacks
 */
internal fun Window.entryCallbackTest() {
	box(Orientation.VERTICAL, 10) {
		Entry().apply {
			addOnActivateCallback {
				println("Entry: activate")
			}
			addOnIconPressCallback { iconPosition, event ->
				println("Entry: icon-press : $iconPosition, $event")
			}
			addOnIconReleaseCallback { iconPosition, event ->
				println("Entry: icon-release : $iconPosition, $event")
			}
		}.also {
			append(it)
		}
	}
}