package org.gtk.gio.ktx

import org.gtk.gio.MenuModel

inline val MenuModel.itemCount: Int
	get() = nItems
