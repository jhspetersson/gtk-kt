package org.gtk.gdk.pixbuf

import kotlinx.cinterop.CPointer
import pixbuf.GdkPixbufAnimation

/**
 * kotlinx-gtk
 * 26 / 03 / 2021
 */
class PixbufAnimation(
	val pixBufAnimationPointer: CPointer<GdkPixbufAnimation>
)