package org.gtk.gobject

import gobject.GTypeInstance
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.pointed
import org.gtk.gobject.TypeClass.Companion.wrap

/**
 * 30 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gobject/struct.TypeInstance.html">
 *     GTypeInstance</a>
 */
interface TypeInstance {
	val typeInstancePointer: CPointer<GTypeInstance>

	val gClass: TypeClass
		get() = typeInstancePointer.pointed.g_class!!.wrap()

}
