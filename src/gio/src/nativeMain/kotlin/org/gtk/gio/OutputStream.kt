package org.gtk.gio

import gio.GOutputStream_autoptr
import gio.g_output_stream_close
import gio.g_output_stream_write_bytes
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.reinterpret
import org.gtk.glib.*
import org.gtk.gobject.KGObject

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gio/method.OutputStream.write_bytes.html">
 *     GOutputStream</a>
 */
open class OutputStream(
	val outputPointer: GOutputStream_autoptr,
) : KGObject(outputPointer.reinterpret()) {

	@Throws(KGError::class)
	fun close(
		cancellable: KGCancellable? = null,
	): Boolean = memScoped {
		val err = allocateGErrorPtr()

		val r = g_output_stream_close(
			outputPointer,
			cancellable?.cancellablePointer,
			err
		)

		err.unwrap()

		r.bool
	}

	@Throws(KGError::class)
	fun writeBytes(
		buffer: KGBytes,
		cancellable: KGCancellable? = null,
	): Long = memScoped {
		val err = allocateGErrorPtr()

		val r = g_output_stream_write_bytes(
			outputPointer,
			buffer.pointer,
			cancellable?.cancellablePointer,
			err
		)

		err.unwrap()

		r
	}
}