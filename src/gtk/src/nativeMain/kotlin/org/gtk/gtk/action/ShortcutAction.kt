package org.gtk.gtk.action

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.Variant
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * gtk-kt
 *
 * 19 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutAction.html">
 *     GtkShortcutAction</a>
 */
open class ShortcutAction(val shortcutAction: GtkShortcutAction_autoptr) :
	KGObject(shortcutAction.reinterpret()) {

	constructor(string: String) :
			this(gtk_shortcut_action_parse_string(string)!!)

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_SHORTCUT_ACTION))

	fun activate(flags: GtkShortcutActionFlags, widget: Widget, args: Variant) =
		gtk_shortcut_action_activate(
			shortcutAction,
			flags,
			widget.widgetPointer,
			args.variantPointer
		).bool

	// ignore gtk_shortcut_action_print C dev function

	override fun toString(): String =
		gtk_shortcut_action_to_string(shortcutAction)!!.toKString()
}