package org.gtk.gtk.widgets

import gobject.GObject
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.TreeListRow
import org.gtk.gtk.TreeListRow.Companion.wrap
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * 29 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.TreeExpander.html">
 *     GtkTreeExpander</a>
 */
class TreeExpander(
	val treeExpanderPointer: GtkTreeExpander_autoptr,
) : KGObject(treeExpanderPointer.reinterpret()) {

	constructor() :
			this(gtk_tree_expander_new()!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_TREE_EXPANDER))

	var child: Widget?
		get() = gtk_tree_expander_get_child(treeExpanderPointer).wrap()
		set(value) = gtk_tree_expander_set_child(
			treeExpanderPointer,
			value?.widgetPointer
		)

	/* TODO Uncomment with 4.6
	/**
	 * @since 4.6
	 */
	var indentForIcon: Boolean
		get() = gtk_tree_expander_get_indent_for_icon(treeExpanderPointer).bool
		set(value) = gtk_tree_expander_set_indent_for_icon(treeExpanderPointer, value.gtk)
	*/

	val item: KGObject?
		get() = gtk_tree_expander_get_item(treeExpanderPointer)
			?.reinterpret<GObject>()
			.wrap()

	var listRow: TreeListRow?
		get() = gtk_tree_expander_get_list_row(treeExpanderPointer).wrap()
		set(value) = gtk_tree_expander_set_list_row(
			treeExpanderPointer,
			value?.treeListRowPointer
		)
}