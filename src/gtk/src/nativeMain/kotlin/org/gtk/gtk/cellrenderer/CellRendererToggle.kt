package org.gtk.gtk.cellrenderer

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRendererToggle.html">
 *     GtkCellRendererToggle</a>
 */
class CellRendererToggle(
	val togglePointer: GtkCellRendererToggle_autoptr,
) : CellRenderer(togglePointer.reinterpret()) {

	constructor() : this(gtk_cell_renderer_toggle_new()!!.reinterpret())

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(
		widget,
		GTK_TYPE_CELL_RENDERER_TOGGLE
	))

	var activatable: Boolean
		get() = gtk_cell_renderer_toggle_get_activatable(
			togglePointer
		).bool
		set(value) = gtk_cell_renderer_toggle_set_activatable(
			togglePointer,
			value.gtk
		)

	var active: Boolean
		get() = gtk_cell_renderer_toggle_get_active(
			togglePointer
		).bool
		set(value) = gtk_cell_renderer_toggle_set_active(
			togglePointer,
			value.gtk
		)

	var radio: Boolean
		get() = gtk_cell_renderer_toggle_get_radio(
			togglePointer
		).bool
		set(value) = gtk_cell_renderer_toggle_set_radio(
			togglePointer,
			value.gtk
		)

	fun addOnToggledCallback(action: CellRendererToggleToggledFunc) =
		addSignalCallback(Signals.TOGGLED, action, staticToggledCallback)

	companion object {
		inline fun GtkCellRendererToggle_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellRendererToggle_autoptr.wrap() =
			CellRendererToggle(this)

		private val staticToggledCallback: GCallback =
			staticCFunction {
					self: GtkCellRendererToggle_autoptr,
					arg1: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<CellRendererToggleToggledFunc>()
					.get()
					.invoke(self.wrap(), arg1.toKString())
			}.reinterpret()
	}
}

typealias CellRendererToggleToggledFunc =
		CellRendererToggle.(
			path: String,
		) -> Unit