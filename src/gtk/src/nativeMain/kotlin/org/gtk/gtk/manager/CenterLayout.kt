package org.gtk.gtk.manager

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.common.enums.BaselinePosition
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CenterLayout.html">
 *     GtkCenterLayout</a>
 */
class CenterLayout(val centerLayoutPointer: GtkCenterLayout_autoptr) :
	LayoutManager(centerLayoutPointer.reinterpret()) {

	constructor() : this(gtk_center_layout_new()!!.reinterpret())

	var baseLinePosition: BaselinePosition
		get() = BaselinePosition.valueOf(
			gtk_center_layout_get_baseline_position(centerLayoutPointer)
		)
		set(value) = gtk_center_layout_set_baseline_position(
			centerLayoutPointer,
			value.gtk
		)

	var centerWidget: Widget?
		get() = gtk_center_layout_get_center_widget(centerLayoutPointer).wrap()
		set(value) = gtk_center_layout_set_center_widget(
			centerLayoutPointer,
			value?.widgetPointer
		)

	var endWidget: Widget?
		get() = gtk_center_layout_get_end_widget(centerLayoutPointer).wrap()
		set(value) = gtk_center_layout_set_end_widget(
			centerLayoutPointer,
			value?.widgetPointer
		)

	var orientation: Orientation
		get() = Orientation.valueOf(gtk_center_layout_get_orientation(centerLayoutPointer))
		set(value) = gtk_center_layout_set_orientation(centerLayoutPointer, value.gtk)

	var startWidget: Widget?
		get() = gtk_center_layout_get_start_widget(centerLayoutPointer).wrap()
		set(value) = gtk_center_layout_set_start_widget(
			centerLayoutPointer,
			value?.widgetPointer
		)
}