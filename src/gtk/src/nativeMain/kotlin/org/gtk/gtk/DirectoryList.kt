package org.gtk.gtk

import gio.GListModel
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.gio.ListModel
import org.gtk.glib.KGError
import org.gtk.glib.KGError.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 27 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DirectoryList.html">
 *     GtkDirectoryList</a>
 */
class DirectoryList(val dirListPointer: CPointer<GtkDirectoryList>) :
	KGObject(dirListPointer.reinterpret()), ListModel {
	override val listModelPointer: CPointer<GListModel> by lazy {
		dirListPointer.reinterpret()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.DirectoryList.new.html">
	 *     gtk_directory_list_new</a>
	 */
	constructor(attributes: String?, file: File?) :
			this(gtk_directory_list_new(attributes, file?.filePointer)!!)

	var attributes: String?
		get() = gtk_directory_list_get_attributes(dirListPointer)?.toKString()
		set(value) = gtk_directory_list_set_attributes(dirListPointer, value)

	val error: KGError?
		get() = gtk_directory_list_get_error(dirListPointer).wrap()

	var file: File?
		get() = gtk_directory_list_get_file(dirListPointer).wrap()
		set(value) = gtk_directory_list_set_file(
			dirListPointer,
			value?.filePointer
		)

	var ioPriority: Int
		get() = gtk_directory_list_get_io_priority(dirListPointer)
		set(value) = gtk_directory_list_set_io_priority(dirListPointer, value)

	var monitored: Boolean
		get() = gtk_directory_list_get_monitored(dirListPointer).bool
		set(value) = gtk_directory_list_set_monitored(
			dirListPointer,
			value.gtk
		)

	val isLoading: Boolean
		get() = gtk_directory_list_is_loading(dirListPointer).bool
}