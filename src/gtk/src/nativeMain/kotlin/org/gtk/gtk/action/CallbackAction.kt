package org.gtk.gtk.action

import gtk.GTK_TYPE_CALLBACK_ACTION
import gtk.GtkCallbackAction_autoptr
import gtk.gtk_callback_action_new
import kotlinx.cinterop.reinterpret
import org.gtk.glib.asStablePointer
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.staticDestroyStableRefFunction
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.common.callback.ShortcutFunction
import org.gtk.gtk.common.callback.staticShortcutFunction

/**
 * gtk-kt
 *
 * 20 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CallbackAction.html">
 *     GtkCallbackAction</a>
 */
class CallbackAction(val callbackAction: GtkCallbackAction_autoptr) :
	ShortcutAction(callbackAction.reinterpret()) {

	constructor(action: ShortcutFunction) : this(
		gtk_callback_action_new(
			staticShortcutFunction,
			action.asStablePointer(),
			staticDestroyStableRefFunction
		)!!.reinterpret()
	)

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_CALLBACK_ACTION))
}