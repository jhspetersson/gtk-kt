package org.gtk.gtk.itemfactory

import gtk.GTK_TYPE_LIST_ITEM_FACTORY
import gtk.GtkListItemFactory_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ListItemFactory.html">
 *     GtkListItemFactory</a>
 */
open class ListItemFactory(
	val listItemFactoryPointer: GtkListItemFactory_autoptr
) : KGObject(listItemFactoryPointer.reinterpret()) {

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_LIST_ITEM_FACTORY
	))

	companion object{
		inline fun GtkListItemFactory_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkListItemFactory_autoptr.wrap() =
			ListItemFactory(this)
	}
}