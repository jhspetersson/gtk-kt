package org.gtk.gtk

import glib.GError_autoptr
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gio.File
import org.gtk.glib.KGError
import org.gtk.glib.KGError.Companion.wrap
import org.gtk.gobject.*
import org.gtk.gtk.CssSection.Companion.wrap

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CssProvider.html">
 *     GtkCssProvider</a>
 */
class CssProvider(
	val cssProviderPointer: GtkCssProvider_autoptr,
) : KGObject(cssProviderPointer.reinterpret()), StyleProvider {
	override val styleProviderPointer: CPointer<GtkStyleProvider> by lazy {
		cssProviderPointer.reinterpret()
	}

	constructor() : this(gtk_css_provider_new()!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_CSS_PROVIDER))

	fun loadFromData(data: String) {
		gtk_css_provider_load_from_data(cssProviderPointer, data, -1)
	}

	fun loadFromFile(file: File) {
		gtk_css_provider_load_from_file(cssProviderPointer, file.filePointer)
	}

	fun loadFromPath(path: String, isResourcePath: Boolean = false) {
		if (isResourcePath) {
			gtk_css_provider_load_from_resource(cssProviderPointer, path)
		} else {
			gtk_css_provider_load_from_path(cssProviderPointer, path)
		}
	}

	fun loadNamed(name: String, variant: String) {
		gtk_css_provider_load_named(cssProviderPointer, name, variant)
	}

	override fun toString(): String =
		gtk_css_provider_to_string(cssProviderPointer)!!.toKString()

	fun addOnParsingErrorCallback(action: CssProviderParsingErrorFunc) =
		addSignalCallback(Signals.PARSING_ERROR, action, staticParsingErrorFunc)

	companion object {

		inline fun GtkCssProvider_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCssProvider_autoptr.wrap() =
			CssProvider(this)

		private val staticParsingErrorFunc: GCallback =
			staticCFunction {
					self: GtkCssProvider_autoptr,
					section: CPointer<GtkCssSection>,
					error: GError_autoptr,
					data: gpointer,
				->
				data.asStableRef<CssProviderParsingErrorFunc>()
					.get()
					.invoke(
						self.wrap(),
						section.wrap(),
						error.wrap()
					)
			}.reinterpret()
	}
}

typealias CssProviderParsingErrorFunc =
		CssProvider.(
			section: CssSection,
			error: KGError,
		) -> Unit