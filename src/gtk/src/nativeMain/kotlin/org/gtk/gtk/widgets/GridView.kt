package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.itemfactory.ListItemFactory
import org.gtk.gtk.itemfactory.ListItemFactory.Companion.wrap
import org.gtk.gtk.selection.SelectionModel
import org.gtk.gtk.selection.SelectionModel.Companion.wrap

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GridView.html">
 *     GtkGridView</a>
 */
class GridView(
	val gridViewPointer: CPointer<GtkGridView>,
) : ListBase(gridViewPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_GRID_VIEW))

	constructor(model: SelectionModel?, factory: ListItemFactory?)
			: this(
		gtk_grid_view_new(
			model?.selectionModelPointer,
			factory?.listItemFactoryPointer
		)!!.reinterpret()
	)

	var isRubberbandEnabled: Boolean
		get() = gtk_grid_view_get_enable_rubberband(gridViewPointer).bool
		set(value) = gtk_grid_view_set_enable_rubberband(gridViewPointer, value.gtk)

	var factory: ListItemFactory?
		get() = gtk_grid_view_get_factory(gridViewPointer).wrap()
		set(value) = gtk_grid_view_set_factory(gridViewPointer, value?.listItemFactoryPointer)

	var maxColumns: UInt
		get() = gtk_grid_view_get_max_columns(gridViewPointer)
		set(value) = gtk_grid_view_set_max_columns(gridViewPointer, value)

	var minColumns: UInt
		get() = gtk_grid_view_get_min_columns(gridViewPointer)
		set(value) = gtk_grid_view_set_min_columns(gridViewPointer, value)

	var model: SelectionModel?
		get() = gtk_grid_view_get_model(gridViewPointer).wrap()
		set(value) = gtk_grid_view_set_model(gridViewPointer, value?.selectionModelPointer)

	var singleClickActivate: Boolean
		get() = gtk_grid_view_get_single_click_activate(gridViewPointer).bool
		set(value) = gtk_grid_view_set_single_click_activate(gridViewPointer, value.gtk)

	fun addOnActivateCallback(action: GridViewActivateFunction) =
		addSignalCallback(Signals.ACTIVATE, action, staticActivateFunction)

	companion object {
		inline fun GtkGridView_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGridView_autoptr.wrap() =
			GridView(this)

		private val staticActivateFunction: GCallback =
			staticCFunction {
					self: GtkGridView_autoptr,
					position: UInt,
					data: gpointer,
				->
				data.asStableRef<GridViewActivateFunction>()
					.get()
					.invoke(self.wrap(), position)
			}.reinterpret()
	}
}

typealias GridViewActivateFunction = GridView.(position: UInt) -> Unit