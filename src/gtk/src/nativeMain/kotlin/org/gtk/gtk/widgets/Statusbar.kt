package org.gtk.gtk.widgets

import glib.gpointer
import glib.guint
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.glib.CStringPointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * 25 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Statusbar.html">
 *     GtkStatusbar</a>
 */
class Statusbar(
	val statusbarPointer: GtkStatusbar_autoptr,
) : Widget(statusbarPointer.reinterpret()) {
	constructor() : this(gtk_statusbar_new()!!.reinterpret())

	fun getContextId(contextDescription: String): UInt =
		gtk_statusbar_get_context_id(statusbarPointer, contextDescription)

	fun pop(contextId: UInt) {
		gtk_statusbar_pop(statusbarPointer, contextId)
	}

	fun push(contextId: UInt, text: String) {
		gtk_statusbar_push(statusbarPointer, contextId, text)
	}

	fun remove(contextId: UInt, messageId: UInt) {
		gtk_statusbar_remove(statusbarPointer, contextId, messageId)
	}

	fun removeAll(contextId: UInt) {
		gtk_statusbar_remove_all(statusbarPointer, contextId)
	}

	fun addOnTextPoppedCallback(action: StatusbarTextChangeFunc) =
		addSignalCallback(Signals.TEXT_POPPED, action, staticTextChangeFunc)

	fun addOnTextPushedCallback(action: StatusbarTextChangeFunc) =
		addSignalCallback(Signals.TEXT_PUSHED, action, staticTextChangeFunc)

	companion object {
		inline fun GtkStatusbar_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkStatusbar_autoptr.wrap() =
			Statusbar(this)

		private val staticTextChangeFunc: GCallback =
			staticCFunction {
					self: GtkStatusbar_autoptr,
					context_id: guint,
					text: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<StatusbarTextChangeFunc>()
					.get()
					.invoke(self.wrap(), context_id, text.toKString())
			}.reinterpret()
	}
}

typealias StatusbarTextChangeFunc =
		Statusbar.(
			contextId: UInt,
			text: String,
		) -> Unit