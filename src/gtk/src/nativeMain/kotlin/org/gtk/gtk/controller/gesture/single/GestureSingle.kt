package org.gtk.gtk.controller.gesture.single

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.EventSequence
import org.gtk.gdk.EventSequence.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.controller.gesture.Gesture

/**
 * gtk-kt
 *
 * 26 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GestureSingle.html">
 *     GtkGestureSingle</a>
 */
open class GestureSingle(val gestureSinglePointer: CPointer<GtkGestureSingle>) :
	Gesture(gestureSinglePointer.reinterpret()) {

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_SINGLE))

	var button: UInt
		get() = gtk_gesture_single_get_button(gestureSinglePointer)
		set(value) = gtk_gesture_single_set_button(gestureSinglePointer, value)

	val currentButton: UInt
		get() = gtk_gesture_single_get_current_button(gestureSinglePointer)

	val currentSequence: EventSequence?
		get() = gtk_gesture_single_get_current_sequence(
			gestureSinglePointer
		).wrap()

	var exclusive: Boolean
		get() = gtk_gesture_single_get_exclusive(gestureSinglePointer).bool
		set(value) = gtk_gesture_single_set_exclusive(gestureSinglePointer, value.gtk)

	var touchOnly: Boolean
		get() = gtk_gesture_single_get_touch_only(gestureSinglePointer).bool
		set(value) = gtk_gesture_single_set_touch_only(gestureSinglePointer, value.gtk)
}