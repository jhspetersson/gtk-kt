package org.gtk.gtk.controller.gesture.single

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.Signals
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.common.enums.PanDirection

/**
 * 03 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GesturePan.html">
 *     GtkGesturePan</a>
 */
class GesturePan(
	val panPointer: GtkGesturePan_autoptr,
) : GestureDrag(panPointer.reinterpret()) {

	constructor(
		orientation: Orientation,
	) : this(gtk_gesture_pan_new(orientation.gtk)!!.reinterpret())

	constructor(obj: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(obj, GTK_TYPE_GESTURE_PAN))

	var orientation: Orientation
		get() = Orientation.valueOf(gtk_gesture_pan_get_orientation(panPointer))
		set(value) = gtk_gesture_pan_set_orientation(panPointer, value.gtk)

	fun addOnPanCallback(action: GesturePanFunc) =
		addSignalCallback(Signals.PAN, action, staticPanFunc)

	companion object {

		inline fun GtkGesturePan_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGesturePan_autoptr.wrap() =
			GesturePan(this)

		private val staticPanFunc: GCallback =
			staticCFunction {
					self: GtkGesturePan_autoptr,
					direction: GtkPanDirection,
					offset: Double,
					data: gpointer,
				->
				data.asStableRef<GesturePanFunc>()
					.get()
					.invoke(
						self.wrap(),
						PanDirection.valueOf(direction),
						offset
					)
			}.reinterpret()
	}
}

typealias GesturePanFunc =
		GesturePan.(
			direction: PanDirection,
			offset: Double,
		) -> Unit