package org.gtk.gtk

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gobject.KGObject
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.selection.SelectionModel
import org.gtk.gtk.selection.SelectionModel.Companion.wrap

/**
 * 04 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SelectionFilterModel.html">
 *     GtkSelectionFilterModel</a>
 */
class SelectionFilterModel(
	val selectionFilterModelPointer: GtkSelectionFilterModel_autoptr,
) : KGObject(selectionFilterModelPointer.reinterpret()), ListModel {
	override val listModelPointer: GListModel_autoptr by lazy {
		selectionFilterModelPointer.reinterpret()
	}

	constructor(model: SelectionModel) :
			this(
				gtk_selection_filter_model_new(
					model.selectionModelPointer
				)!!.reinterpret()
			)

	constructor(obj: TypeInstance) : this(typeCheckInstanceCastOrThrow(
		obj,
		GTK_TYPE_SELECTION_FILTER_MODEL
	))


	var model: SelectionModel?
		get() = gtk_selection_filter_model_get_model(
			selectionFilterModelPointer
		).wrap()
		set(value) = gtk_selection_filter_model_set_model(
			selectionFilterModelPointer,
			value?.selectionModelPointer
		)
}