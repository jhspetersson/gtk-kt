package org.gtk.graphene

import gtk.graphene_size_t
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 07 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class Size(val sizePointer:CPointer<graphene_size_t>) {
}