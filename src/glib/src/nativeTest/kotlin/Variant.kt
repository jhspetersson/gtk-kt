import org.gtk.glib.Variant
import org.gtk.glib.use
import kotlin.test.Test

/*
 * gtk-kt
 *
 * 17 / 10 / 2021
 */

@Test
fun StringVariantTest() {
	val kotlin = "Meow"
	Variant(kotlin).use {
		assert(kotlin == it.string) {
			"Strings did not match"
		}
	}

}

@Test
fun ShortVariantTest() {
	val kotlin: Short = 29
	Variant(kotlin).use {
		assert(kotlin == it.short) {
			"Shorts did not match"
		}
	}

}

@Test
fun UShortVariantTest() {
	val kotlin: UShort = 29u
	Variant(kotlin).use {
		assert(kotlin == it.uShort) {
			"UShorts did not match"
		}
	}

}

@Test
fun IntVariantTest() {
	val kotlin = 29
	Variant(kotlin).use {
		assert(kotlin == it.int) {
			"Ints did not match"
		}
	}

}

@Test
fun UIntVariantTest() {
	val kotlin = 29u
	Variant(kotlin).use {
		assert(kotlin == it.uInt) {
			"UInts did not match"
		}
	}

}

@Test
fun LongVariantTest() {
	val kotlin = 29L
	Variant(kotlin).use {
		assert(kotlin == it.long) {
			"Longs did not match"
		}
	}

}

@Test
fun ULongVariantTest() {
	val kotlin = 29uL
	Variant(kotlin).use {
		assert(kotlin == it.uLong) {
			"ULongs did not match"
		}
	}
}
