plugins {
	kotlin("multiplatform")
	`maven-publish`
	signing
	`dokka-script`
}

version = "0.1.0-alpha1"
description = "Kotlin bindings for GLib, targeting 2.70.2"

kotlin {
	native {
		val main by compilations.getting
		val glib by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}
}

apply(plugin = "native-publish")